import twitter_scraper, requests, json, os
from bs4 import BeautifulSoup
import config, atasco

# For a new login
if not os.path.isfile('cache_twitter.json'):
    print('Logging in...')

    # Create a requests session to have persistant cookies
    session = requests.Session()
    # First request to get the cookies and token
    login_page = session.get('https://twitter.com/login')
    # The token is a string that has to be passed on the login which is included as a field in the form
    authenticity_token = BeautifulSoup(login_page.text, 'html.parser').find('input').get('value')
    
    login_data = {
        'session[username_or_email]': config.twitter_username,
        'session[password]': config.twitter_password,
        'authenticity_token': authenticity_token,
        'remember_me': '1'
    }
    
    # Actually send the login data
    session.post('https://twitter.com/sessions', data=login_data)

    # Save the cookies for next time
    with open('cache_twitter.json', 'w+') as outfile:
        json.dump(requests.utils.dict_from_cookiejar(session.cookies), outfile)

else:
    # Load the saved cookies from the previous time
    with open('cache_twitter.json') as file_data:
       cookies = json.load(file_data)
    print('Loaded cookies')

# Load list of already commented post to not repeat
try:
    with open('ac_twitter.json') as file_data:
        already_commented = json.load(file_data)
except FileNotFoundError as e:
    already_commented = []

for tweet in twitter_scraper.get_tweets('AlmeidaPP_', pages=1):
    if not tweet['isRetweet']:
        id = tweet['tweetId']
        if not id in already_commented:
            already_commented.append(id)
            print(f'Tweeting on {id}...')
            comment_data = {
                'status': atasco.atasco(140),
                'in_reply_to_status_id': id,
                'auto_populate_reply_metadata': 'true' # To add the mentions automatically
            }
            # Send the reply tweet. the header is needed for some reason i don't know
            requests.post('https://twitter.com/i/tweet/create', cookies=cookies, headers={'referer': 'https://twitter.com/'}, data=comment_data)
        else:
            print(f'Skipping {id}')

# Save new list for the next use
with open('ac_twitter.json', 'w+') as outfile:
    json.dump(already_commented, outfile)
