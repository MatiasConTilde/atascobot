from instagram_private_api import Client, ClientCookieExpiredError, ClientLoginRequiredError
import json, codecs, os, random
import config, atasco

# All this first part is to save cookies and such to not have to login again every time
# Followed the example from https://github.com/ping/instagram_private_api/blob/master/examples/savesettings_logincallback.py

def to_json(python_object):
    if isinstance(python_object, bytes):
        return {'__class__': 'bytes',
                '__value__': codecs.encode(python_object, 'base64').decode()}
    raise TypeError(repr(python_object) + ' is not JSON serializable')

def from_json(json_object):
    if '__class__' in json_object and json_object['__class__'] == 'bytes':
        return codecs.decode(json_object['__value__'].encode(), 'base64')
    return json_object

def onlogin_callback(api):
    cache_settings = api.settings
    with open('cache_insta.json', 'w+') as outfile:
        json.dump(cache_settings, outfile, default=to_json)

device_id = None
try:
    if not os.path.isfile('cache_insta.json'):
        # settings file does not exist
        print('Unable to find settings!')

        # login new
        api = Client(
            config.insta_username, config.insta_password,
            on_login=lambda x: onlogin_callback(x))

    else:
        with open('cache_insta.json') as file_data:
            cached_settings = json.load(file_data, object_hook=from_json)
        print('Loaded settings')

        device_id = cached_settings.get('device_id')

        api = Client(
            config.insta_username, config.insta_password,
            settings=cached_settings)

except (ClientCookieExpiredError, ClientLoginRequiredError) as e:
    print(f'ClientCookieExpiredError/ClientLoginRequiredError: {e}')

    # Login expired
    # Do relogin but use default ua, keys and such
    api = Client(
        config.insta_username, config.insta_password,
        device_id=device_id,
        on_login=lambda x: onlogin_callback(x))

# Load list of already commented post to not repeat
try:
    with open('already_commented.json') as file_data:
        already_commented = json.load(file_data)
except FileNotFoundError as e:
    already_commented = []

carapolla_id = 5951005151 # @martinez_almeida_

for post in api.user_feed(carapolla_id)['items']:
    id = post['pk']
    if not id in already_commented:
        print(f'Commenting on {id}')
        api.post_comment(id, atasco.atasco(random.randint(100, 300)))
        already_commented.append(id)
    else:
        print(f'Skipping {id}')

# Save new list for the next use
with open('already_commented.json', 'w+') as outfile:
    json.dump(already_commented, outfile)
