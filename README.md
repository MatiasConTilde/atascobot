# AtascoBot

> 🚛🚙🚌🚔🚘🚑🚐🚛🚍🚔🚒🚓🚑🚔🏍🚚🏍🚖🚓🏎🚚🚙🚗🚔🚘🚛🚍🚚🏍🛵🚔🚍🚔🚓🚘🚔🚚🚛🚘🚔🚘🚚🚕🚑🚌🚙🚒🚕🚖🚍🚖🚓🚔🚒🏍🚕🚓🚌🚌🚛🏍🚘🚘🚚🚐🏎🚒🚐🚕🚛🚗🚔🚌🚛🚕🚕🚕🚔🚛🚌🚒🚓🚛🛵🚑🚚🚚🛵🛵🚕🚛🚚🚍🏍🚗🚛🚑🚓🚛🚛🚙🛵🚕🚓🚙🚐🚙🏍🚔🚔🚑🏍🚗🚒🚒🚛🚙🚖🚓🏍🚖🚗🚔🚙🏎🚕🏎🚕

Debido a la genial idea de nuestro querido alcalde <abbr title='Carapolla'>Almeida</abbr> de <a href='https://lee.eldiario.es/almeida-coche'>revertir Madrid Central</a>, se han creado infinidades de atascos en el centro de la capital, mucho más grandes de los que <a href='https://twitter.com/AlmeidaPP_/status/1124240469133877249'>se quejaba él mismo anteriormente</a>. Como método de protesta la gente <a href='https://lee.eldiario.es/almeidaatascos'>llenó sus posts en redes sociales de atascos de emojis</a>. Para hacerte este trabajo tan tedioso más fácil, he creado estos bots para redes sociales que lo hacen por ti.

## Instalación

```bash
git clone https://gitlab.com/MatiasConTilde/atascobot.git
cd atascobot
pip install git+https://git@github.com/ping/instagram_private_api.git@1.6.0 #solo hace falta si vas a usar Instagram
pip install git+https://git@github.com/kennethreitz/twitter-scraper.git #solo hace falta si vas a usar Twitter
```

## Uso

- Rellena `config.py` con tus credenciales (solo hacen falta para las redes sociales que vayas a usar)
- `python insta.py` para Instagram o `python twitter.py` para Twitter. Idealmente añádelo a un cronjob para que se ejecute regularmente y comentes en todas las fotos nuevas

## Disclaimer

Técnicamente eso de los bots esta un poco prohibidos pero con estos scripts existe muy bajo riesgo de que le pase nada a tu cuenta ya que no expone ninguna actividad sospechosa que pueda parecer automatizada. También parece ser que al señor alcalde no le gusta tanto esta campaña de troll por lo que <a href='http://lee.eldiario.es/almeidacomentarios'>ha bloqueado los comentarios a veces</a> así que no sé cuanto va a aguantar este bot funcionando pero mientras tanto va bien así que aprovecha jajaja.
