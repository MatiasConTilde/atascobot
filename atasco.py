import random

def atasco(length):
    cars = ['🚗', '🚕', '🚙', '🚌', '🏎', '🚓', '🚑', '🚒', '🚐', '🚚', '🚛', '🛵', '🏍', '🚔', '🚍', '🚘', '🚖']
    text = ''

    for i in range(length):
        text += random.choice(cars)

    return text
